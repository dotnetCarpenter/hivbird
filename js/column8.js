/**
 * @version 2013.10.1
 * @author jon.ronnenberg@gmail.com
 */

var hBird = hBird || {};

hBird.iconTable = {
  "Undiagnosed": "drop",
  "Diagnosed, not on ART": "pipette",
  "Lost to follow-up": "question",
  "On ART": "pill",
  "Diseased": "cross"
}

hBird.create = function create(config) {
  "use strict";
  for(var prop in config) {
    if(config.hasOwnProperty(prop))
      this[prop] = config[prop];
  }
};

hBird.load = function(cb, config) {
  "use strict";
  var mimeType = /\.(svg|json)/,
      found = mimeType.exec(config.url);
  switch ( ((found && found[1]) || "") ) {
    case "svg":
      d3.xml(config.url, "image/svg+xml", function(error, xml) {
        if(error)
          return console.warn(error)
        config.iconset = xml;
        cb(config);
      });
      break;
    case "json":
      d3.json(config.url, function(error, data) {
        if(error)
          return console.warn(error)
        config.data = data;
        cb(config);
      });
      break;
    default:
      console.warn("hBird.load: mime-type not found - nothing is loaded");
      cb(config);
  }
};

hBird.addValue = function(modifier) {
  "use strict";
  var firstValue = 0;
  if(modifier && !(modifier instanceof Function))
    throw "modifier must be a function";
  return function(item) {
    return modifier ?
            modifier(firstValue += item.value) :
            firstValue += item.value;
  }
};

hBird.sizes = {
  width: d3.select("article").node().clientWidth,
  height: 675,
  calcWidth: function getWidth(value) {
    var width = parseFloat(this.getFloat.exec(value)[0]);
    // console.log("percent: " + value, "parsed: " + this.getFloat.exec(value)[0], "article width: " + this.width);
    // console.log(Math.round( this.isPercentage.test(String(value)) ? this.width * width / 100 : width));
    return Math.round( this.isPercentage.test(String(value)) ? this.width * width / 100 : width);
  },
  calcHeight: function getHeight(value) {
    var height = parseFloat(this.getFloat.exec(value)[0]);
    // console.log("percent: " + value, "parsed: " + this.getFloat.exec(value)[0], "article height: " + this.height);
    // console.log(Math.round( this.isPercentage.test(String(value)) ? this.height * height / 100 : height));
    return Math.round( this.isPercentage.test(String(value)) ? this.height * height / 100 : height);
  },
  isPercentage: /%$/,
  getFloat: /\d+\.?\d+/,
  margin: { top: 16, legendRight: 78, right: 390, bottom: 16, left: 148 },
  barWidth: 0
};

hBird.yDownUpScale = d3.scale.linear()
                .domain([0, 100])
                .range([hBird.sizes.height - hBird.sizes.margin.bottom, hBird.sizes.margin.top]);
// TODO: setup proper scales <http://www.d3noob.org/2012/12/setting-scales-domains-and-ranges-in.html>
hBird.yScale = d3.scale.linear()
                .domain([0, 100])
                .range([0, hBird.sizes.height - hBird.sizes.margin.bottom]);

hBird.yCoords = function(data) {
    "use strict";
  var intermed = [0].concat(
    data.map(
      hBird.addValue(hBird.yScale)
    )
  );
  return hBird.yCoords.coords = intermed.splice(0, intermed.length-1);
};

hBird.median = function(i) {
  "use strict";
  var coords = hBird.yCoords.coords,
      next = i === coords.length-1 ? (hBird.sizes.height - hBird.sizes.margin.bottom) : coords[i+1];
  return (hBird.yCoords.coords[i] + next) / 2;
};

hBird.indexOf = function(list, fn) {
  "use strict";
  for (var i = 0, len = list.length; i < len; i++) {
    if(fn(list[i], i, list))
      return i;
  }
  return -1;
};

hBird.initialize = function(cb, config) {
  "use strict";
      var yC = hBird.yCoords(config.data.values);
    var size = hBird.sizes,
      canvas = d3.select(".chart")
                 .append("svg");
  canvas.attr("viewBox", "0 0 762 675")
        .attr("preserveAspectRatio", "xMinYMin meet");
    /* Gradients */
    var defs = canvas.append("defs"),
   tmp = defs.append("linearGradient")
                .attr("id", "gradient0");
   tmp.append("stop")
                .attr("stop-color", "rgb(63,108,122)")
                .attr("offset", "0");
   tmp.append("stop")
                .attr("stop-color", "rgb(81,140,163)")
                .attr("offset", "100%");
   tmp = defs.append("linearGradient")
                .attr("id", "gradient1");
   tmp.append("stop")
                .attr("stop-color", "rgb(0,62,86)")
                .attr("offset", "0");
   tmp.append("stop")
                .attr("stop-color", "rgb(0,87,114)")
                .attr("offset", "100%");
   tmp = defs.append("linearGradient")
                .attr("id", "gradient2");
   tmp.append("stop")
                .attr("stop-color", "rgb(12,137,175)")
                .attr("offset", "0");
   tmp.append("stop")
                .attr("stop-color", "rgb(13,163,221)")
                .attr("offset", "100%");
   tmp = defs.append("linearGradient")
                .attr("id", "gradient3");
   tmp.append("stop")
                .attr("stop-color", "rgb(81,137,150)")
                .attr("offset", "0");
   tmp.append("stop")
                .attr("stop-color", "rgb(107,184,224)")
                .attr("offset", "100%");
   tmp = defs.append("linearGradient")
                .attr("id", "gradient4");
   tmp.append("stop")
                .attr("stop-color", "rgb(81,81,81)")
                .attr("offset", "0");
   tmp.append("stop")
                .attr("stop-color", "rgb(109,109,109)")
                .attr("offset", "100%");
    var filter = defs.append("filter")
              .attr("id", "dropshadow");
    // filter.append( 'feGaussianBlur' )
    //       .attr( 'in', 'SourceAlpha' )
    //       .attr( 'stdDeviation', 2 )
    //       .attr( 'result', 'blur' );
    // filter.append( 'feOffset' )
    //       .attr( 'in', 'blur' )
    //       .attr( 'dx', 2 )
    //       .attr( 'dy', 3 )
    //       .attr( 'result', 'offsetBlur' );
    // tmp =  filter.append( 'feMerge' );
    // tmp.append( 'feMergeNode' )
    //         .attr( 'in", "offsetBlur' )
    // tmp.append( 'feMergeNode' )
    //         .attr( 'in', 'SourceGraphic' );

      filter.append("feGaussianBlur")
      .attr("in", "SourceAlpha")
      .attr("stdDeviation", 5);
    filter.append("feOffset")
        .attr("dx", -8)
        .attr("dy", 2)
        .attr("result", "offsetblur");
    // filter.append("feFlood")
    //     .attr("flood-color", "rgba(0,0,0,0.01)");
    tmp = filter.append("feComponentTransfer");
    tmp.append("feFuncA")
        .attr("type", "linear")
        .attr("slope", "0.25");
    tmp = filter.append("feMerge")
    tmp.append("feMergeNode")
    tmp.append("feMergeNode")
      .attr("in", "SourceGraphic")
  /*<filter id="dropshadow" height="130%">
      <feGaussianBlur in="SourceAlpha" stdDeviation="3"/> 
      <feOffset dx="-9" dy="0" result="offsetblur"/>
      <feComponentTransfer>
        <feFuncA type="linear" slope="0.2"/>
      </feComponentTransfer>
      <feMerge> 
        <feMergeNode/>
        <feMergeNode in="SourceGraphic"/> 
      </feMerge>
    </filter>*/
    /* // filter stuff
    /*
    var defs = svg.append( 'defs' );

    var filter = defs.append( 'filter' )
                      .attr( 'id', 'dropshadow' )

    filter.append( 'feGaussianBlur' )
          .attr( 'in', 'SourceAlpha' )
          .attr( 'stdDeviation', 2 )
          .attr( 'result', 'blur' );
    filter.append( 'feOffset' )
          .attr( 'in', 'blur' )
          .attr( 'dx', 2 )
          .attr( 'dy', 3 )
          .attr( 'result', 'offsetBlur' );

    var feMerge = filter.append( 'feMerge' );

    feMerge.append( 'feMergeNode' )
            .attr( 'in", "offsetBlur' )
    feMerge.append( 'feMergeNode' )
            .attr( 'in', 'SourceGraphic' );
    */
   defs = filter = tmp = null;
size.barWidth = 762 - size.margin.right - size.margin.left;
     var rect = canvas
                .append("g")
                .attr("class", "bar bar" + hBird.indexOf(config.dataset, function(d) {
                  return d.period === config.data.period;                  
                }))
                .selectAll("rect")
                .data(config.data.values)
                .enter()                
                .append("g") 
                .attr("class", "rects")              
                .attr("transform", "translate(" + size.margin.left + ")")
                .append("rect")                
                .attr( 'filter', 'url(#dropshadow)' )
                .attr("fill", function(d, i) {
                  return "url(#gradient" + i + ")";
                })
                .attr("y", function(d, i) {
                  return yC[i];
                })
                .attr("height", function(d) {
                  return hBird.yScale(d.value);
                })
                .attr("width", size.barWidth)
                .attr("class", function(d, i) {
                  return "segment" + (i + 1);
                })
                .each(function(d, i) {
                  this._yCoord = yC[i];
                  this._datum = d;
                });
var lineToLabels = canvas
                .selectAll("g.rects")
                .data(config.data.values)
                .append("line")
                .attr("x1", 0)
                .attr("y1", function(d, i) {
                  return hBird.median(i);
                })
                .attr("x2", size.margin.legendRight)
                .attr("y2", function(d, i) {
                  return hBird.median(i)-30;
                })
                .attr("stroke-width", 1)
                .attr("transform", "translate(" + size.barWidth + ")")
                .attr("class", "line-to-label")
                .each(function(d) {
                  if(!d.value)
                    d3.select(this).attr("opacity", 0);
                });
   var legend = canvas
                .selectAll("g.rects")
                .data(config.data.values)
                .append("g")
                //.attr("transform", "translate(" + (rectWidth + size.margin.legendRight + 22) + ")");
                .attr("transform", function(d, i) {
                  var x = size.barWidth + size.margin.legendRight + 22;
                  return "translate(" + x + ", " + hBird.median(i) + ")";
                })
                .each(function(d) {
                  if(!d.value)
                    d3.select(this).attr("opacity", 0);
                });
var percentage = legend
                .append("text")
                .text(function(d) {
                  return Math.round(d.value) + "%";
                })
                .attr("y", -36)
                .attr("fill", "rgb(107,184,214)")
                .attr("class", "percentage-svg");
    var icons = legend
                .attr("d", function(d) {
                  var iconName = hBird.iconTable[d.label.trim()];
                  var icon = d3.select(config.iconset).select("#" + iconName);
                  icon.attr("class", iconName);
                  switch (iconName) {
                    case "drop":
                      icon.attr("transform", "translate(5, -76)");
                      break;
                    case "pipette":
                      icon.attr("transform", "translate(-26, -79)");
                      break;                    
                    case "question":
                      icon.attr("transform", "translate(-10, -80)");
                      break;
                    case "pill":
                      icon.attr("transform", "translate(-8, -80)");
                      break;
                    case "cross":
                      icon.attr("transform", "translate(-8, -81)");
                      break;
                    default:
                      icon.attr("transform", "translate(0, -69)");
                  }
                  this.appendChild(icon.node()/*.cloneNode(true)*/);
                });
   var labels = legend
                .append("text")
                .text(function(d) {
                  return d.label;
                })
                .attr("y", -10)
                .attr("fill", "#000")
                .attr("text-anchor", "right")
                .attr("class", "label");
    var yAxis = d3.svg.axis()
                .scale(hBird.yDownUpScale)
                .tickSize(1)/*
                .tickPadding(-4)
                .innerTickSize(2)*/
                .orient("left")
                .tickValues([0, 20, 40, 60, 80, 100])
                .tickFormat(function(d) {
                  return d + "%";
                });
  canvas.append("g")
        .attr("class", "y-axis")
        .attr("transform", "translate(60)") /* minimum to see text */
        .call(yAxis);

  var survivalRate = d3.select(".lost .number");
  survivalRate
    .datum(config.data["Survival Rate"])
    .transition()
    .duration(750)
    .delay(100)
    .tween("text", function(d) {
      var orig = +this.textContent,
          inter= d3.interpolateRound(orig, d);
      return function(t) {
        this.textContent = inter(t);
      }
    });
  var periodText = d3.select(".year")
                     .text(config.data.period);

  config.canvas = canvas;
  config.rect = rect;
  config.periodText = periodText;
  config.legend = legend;
  config.labels = labels;
  config.percentage = percentage;
  config.lineToLabels = lineToLabels;
  config.survivalRate = survivalRate;
  cb(config);
};

hBird.cleanup = function(cb, config) {
  config.iconset = null;
  delete config.iconset;
  cb(config);
}

hBird.draw = function draw(cb, config) {
  "use strict";
  hBird.yCoords(config.data.values);
  // set bar g class
  config.canvas
        .select(".bar")
        .attr("class", "bar bar" + hBird.indexOf(config.dataset, function(d) {
          return d.period === config.data.period;                  
        }));
  config.rect
        .data(config.data.values)
        .transition()
        .duration(750)
        .attrTween("y", function(d, i) {
          var inter = d3.interpolate(this._yCoord, hBird.yCoords.coords[i]);
          this._yCoord = hBird.yCoords.coords[i];
          return inter;
        })
        .attrTween("height", function(d) {
          var inter = d3.interpolate(hBird.yScale(this._datum.value), hBird.yScale(d.value));
          this._datum = d;
          return inter;
        })
        .each(function(d, i) {
          d3.select(this)
            .attr("height", hBird.yScale(d.value))
            .attr("y", hBird.yCoords.coords[i]);
        });
  
  config.periodText.text(config.data.period);
  config.legend
        .data(config.data.values)
        .transition()
        .duration(750)
        .attr("transform", function(d, i) {
          var size = hBird.sizes;
          var x = size.barWidth + size.margin.legendRight + 22;
          return "translate(" + x + ", " + hBird.median(i) + ")";
        })
        .attr("opacity", function(d) {
          return d.value ? 1 : 0;
        })
        .select(".drop")
        .attr("transform", function(d) {
          return d.value === 100 ? "translate(2, -76)" : "translate(-14, -76)";
        });
  config.percentage
        .data(config.data.values)
        .transition()
        .duration(750)
        .tween("text", function(d) {
          var orig = +this.textContent.replace("%", ""),
              //inter= d3.interpolateNumber(orig, d.value),
              //isInt= d.value % 1 === 0;
              inter= d3.interpolateRound(orig, d.value);              
          return function(t) {
            var display = inter(t);
            //this.textContent = (isInt ? inter(t).toFixed(0) : inter(t).toFixed(1)) + "%";
            this.textContent = inter(t) + "%";
          }
        });
  config.lineToLabels
        .data(config.data.values)
        .transition()
        .duration(750)
        .attr("y1", function(d, i) {
          return hBird.median(i);
        })
        .attr("y2", function(d, i) {
          // return the median between the current and next coord
          return hBird.median(i)-30;
        })
        .attr("opacity", function(d) {
          return d.value ? 1 : 0;
        });
  
  config.survivalRate
    .datum(config.data["Survival Rate"])
    .transition()
    .duration(750)
    .delay(260)
    .tween("text", function(d) {
      var orig = +this.textContent,
          inter= d3.interpolateRound(orig, d);
      return function(t) {
        this.textContent = inter(t);
      }
    });

  cb(config);
};

hBird.slideOut = function(cb, config) {
  // remove legend and lines
  config.legend
        .transition()
        .duration(750)
        .attr("opacity", 0);
  config.lineToLabels  
        .transition()
        .duration(750)
        .attr("opacity", 0);
  /* widen the svg area */
  config.canvas
        .attr("style", "width:" +
          (hBird.sizes.calcWidth( d3.select(".chart").style("width") ) /*- 160*/)//TODO: recalc 160 to new scale
          + "px");
 // var currentPeriod = +config.data.period;
  var bars = config.canvas
                .selectAll("g.bar")
                .data(config.dataset)
                .enter()
                .append("g")
                .attr( 'filter', 'url(#dropshadow)' );
                // .each(function(d) {
                //   d3.select(this)
                //     .attr("class", function(d, i) {                          
                //       // var name = (currentPeriod >= i*2 ? config.dataset[++i].period : config.dataset[i].period);
                //       // console.log(name);
                //       return "bar bar" + d.period;
                //     });
                // });
    
  bars
    .data(config.dataset)
    .transition()
    .duration(1000)
    .ease(d3.ease("elastic"))
    .each(function(d, i) {
      //var correctData = config.data.period >= i
      var yC = hBird.yCoords(d.values);
      console.log(i);
      d3.select(this)
        .selectAll("rect")
        .data(d.values)
        .enter()
        .append("rect")
        .attr("fill", function(d, i) {
          return "url(#gradient" + i + ")";
        })
        .attr("width", hBird.sizes.barWidth)
        .attr("y", function(d, i) {
          return yC[i];
        })
        .attr("height", function(d) {
          return hBird.yScale(d.value);
        })
      })
    .attr("transform", function(d) {
      // TODO: calc proper multiplier (60)
      return "translate(" + (+d.period*60 + hBird.sizes.margin.left) + ")";
    });
  cb(config);
}

hBird.slideIn = function(cb, config) {

  cb(config);
}

hBird.log = function log(cb, data) {
  console.dir(data);
  cb(data);
};

hBird.previous = function previous(config, back, forw) {
  "use strict";
  return function() {
    // this.style.cursor = "progress";
    // drawing.transition().duration(0);
    hBird.move(-1, config, back, forw);
    // this.style.cursor = "pointer";
  }
};

hBird.next = function next(config, back, forw) {
  "use strict";
  return function() {
    // this.style.cursor = "progress";
    // drawing.transition().duration(0);
    hBird.move(1, config, back, forw);
    // this.style.cursor = "pointer";
  }
};

hBird.update = function(object, giveprop, takeprop, fn) {
  "use strict";
  var argumentsLength = arguments.length;
  return function update(cb, updated) {
    switch (argumentsLength) {
      case 1: object = updated; break;
      case 2: object[giveprop] = updated; break;
      case 3: object[giveprop] = updated[takeprop]; break;
      case 4: object[giveprop] = fn(updated[takeprop]); break;
      default: console.warn("hBird.update called with weird arguments");
    }
    cb(object);
  }
};

hBird.move = function(direction, config, backwards, forwards) {
  "use strict";
  var test = config.counter.c;
  test += direction;
  if(test < 0 || test >= config.counter.max) {
    console.warn("we got to the end!")
    return; // we at the end
  }
  !~direction ? hBird.defer(backwards(config)) : hBird.defer(forwards(config));
};

hBird.pickData = function(direction, counter) {
  "use strict";
  return function pickData(cb, config) {
    config.data = config.dataset[[!~direction ? --counter.c : ++counter.c]];
    cb(config);
  }
};

// Needs further thought
// hBird.indentity = function(cb, data) {
//   cb(cb(data), data);
// };
hBird.wait = function(wait) {
  "use strict";
  return function wait(cb, config) {
    setTimeout(function() {
      cb(config);
    }, wait)
  }
};

hBird.defer = function defer() {
  return hBird.wait(0);
}

hBird.noOp = function noOp(cb, config) { cb; };

(function() {
  "use strict";
  var counter = { c:-1, max: 0 },//TODO 0 from url .. -1 is a hack because forwardDataPicker otherwise will initialize the 1st dataset and not the 0th
      forwardDataPicker = hBird.pickData(1, counter),
      backwardsDataPicker = hBird.pickData(-1, counter);

  var svgUrl = function(cb, config) {
    hBird.update(config, "url")(cb, "images/iconset_stacked.svg");
  }
  var changeClass = function(el, to) {
    var el = d3.select(el),
        origClass = el.attr("class");
    return function(cb, config) {
      el.attr("class", origClass + " " + to);
      cb(config);
    }
  }

  /* performance measurement */
  // var start = function(cb, config) {
  //   config.start = window.performance.now();
  //   cb(config);
  // };
  // var end = function(cb, config) {
  //   console.log( window.performance.now() - config.start + "ms");
  //   cb(config);
  // };

  // setup program
  var config = new hBird.create({
    lineToLabels: undefined,
    legend: undefined,
    percentage: undefined,
    labels: undefined,
    periodText: undefined,
    url: "data/data10.json",
    dataset: undefined,
    data: undefined,
    canvas: undefined,
    rect: undefined,
    counter:counter,
    iconset: undefined,
    survivalRate: undefined,
    start: undefined
  });
  var initialRun = hBird.compose(
    // start,
    hBird.load,
    hBird.update(config, "dataset", "data"),
    forwardDataPicker,
    svgUrl,
    hBird.load,
    hBird.initialize,
    // end,
    hBird.update(counter, "max", "dataset", function(d) {
      return d.length;
    }),
    hBird.cleanup    
  );
  var forwards = hBird.composeCycle(
    // start,
    forwardDataPicker,
    hBird.draw,
    // end,
    hBird.noOp
  );
  var backwards = hBird.composeCycle(
    // start,
    backwardsDataPicker,
    hBird.draw,
    // end,
    hBird.noOp
  );
  var survivalRateCircle = document.querySelector(".lost");
  var launchHistogram = hBird.composeCycle(
    // start,
    changeClass(survivalRateCircle, "slide-out"),
    hBird.slideOut,
    // end,
    // hBird.log,
    hBird.noOp
  );
  var cancelHistogram = hBird.composeCycle(
    // start,
    hBird.slideIn,
    changeClass(survivalRateCircle, ""),
    // end,
    // hBird.log,
    hBird.noOp
  );

  // behavior
  d3.select(".previous")
    .on("click", hBird.previous(config, backwards, forwards));
  d3.select(".next")
    .on("click", hBird.next(config, backwards, forwards));
  var fullscreenButton = document.querySelector(".fullscreen");
  if(fullscreenButton) {
    fullscreenButton.addEventListener(
      "click",
      toggle(
        function() {
          // Launch fullscreen for browsers that support it!
          launchFullScreen(document.documentElement);
        },
        cancelFullscreen
      ),
      false
    );
  }
  d3.select(".histogram")
    .on("click", toggle(function() {
      launchHistogram(config);
    },
    function() {
      cancelHistogram(config);
    }));

  // start program
  initialRun(config);


  /* util */
  function toggle (fn1, fn2) {
    var t = true;
    return function() {
      t ? fn1() : fn2();
      t = !t;
    }
  }

  /**
   *  Fullscreen stuff
   */
  //http://davidwalsh.name/fullscreen
  function launchFullScreen(element) {
    if(element.requestFullScreen) {
      element.requestFullScreen();
    } else if(element.mozRequestFullScreen) {
      element.mozRequestFullScreen();
    } else if(element.webkitRequestFullScreen) {
      element.webkitRequestFullScreen();
    }
  }
  function cancelFullscreen() {
    if(document.cancelFullScreen) {
      document.cancelFullScreen();
    } else if(document.mozCancelFullScreen) {
      document.mozCancelFullScreen();
    } else if(document.webkitCancelFullScreen) {
      document.webkitCancelFullScreen();
    }
  }
}());
