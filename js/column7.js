/**
 * @version 2013.10.1
 * @author jon.ronnenberg@gmail.com
 */

var hBird = hBird || {};

hBird.iconTable = {
  "Undiagnosed": "drop",
  "Diagnosed, not on ART": "pipette",
  "Lost to follow-up": "question",
  "On ART": "pill",
  "Diseased": "cross"
}

hBird.create = function create(config) {
  for(var prop in config) {
    if(config.hasOwnProperty(prop))
      this[prop] = config[prop];
  }
};

hBird.load = function(cb, config) {
  var mimeType = /\.(svg|json)/,
      found = mimeType.exec(config.url);
  switch ( ((found && found[1]) || "") ) {
    case "svg":
      d3.xml(config.url, "image/svg+xml", function(error, xml) {
        if(error)
          return console.warn(error)
        config.iconset = xml;
        cb(config);
      });
      break;
    case "json":
      d3.json(config.url, function(error, data) {
        if(error)
          return console.warn(error)
        config.data = data;
        cb(config);
      });
      break;
    default:
      console.warn("hBird.load: mime type not found - nothing is loaded");
      cb(config);
  }
};

hBird.addValue = function(modifier) {
  var firstValue = 0;
  if(modifier && !(modifier instanceof Function))
    throw "modifier must be a function";
  return function(item) {
    return modifier ?
            modifier(firstValue += item.value) :
            firstValue += item.value;
  }
};

hBird.sizes = {
  width: d3.select("article").node().clientWidth,
  height: 675,
  calcWidth: function getWidth(value) {
    var width = parseFloat(this.getFloat.exec(value)[0]);
    // console.log("percent: " + value, "parsed: " + this.getFloat.exec(value)[0], "article width: " + this.width);
    // console.log(Math.round( this.isPercentage.test(String(value)) ? this.width * width / 100 : width));
    return Math.round( this.isPercentage.test(String(value)) ? this.width * width / 100 : width);
  },
  calcHeight: function getHeight(value) {
    var height = parseFloat(this.getFloat.exec(value)[0]);
    // console.log("percent: " + value, "parsed: " + this.getFloat.exec(value)[0], "article height: " + this.height);
    // console.log(Math.round( this.isPercentage.test(String(value)) ? this.height * height / 100 : height));
    return Math.round( this.isPercentage.test(String(value)) ? this.height * height / 100 : height);
  },
  isPercentage: /%$/,
  getFloat: /\d+\.?\d+/,
  margin: { top: 16, legendRight: 78, right: 390, bottom: 16, left: 148 },
  barWidth: 0
};

hBird.yDownUpScale = d3.scale.linear()
                .domain([0, 100])
                .range([hBird.sizes.height - hBird.sizes.margin.bottom, hBird.sizes.margin.top]);
// TODO: setup proper scales <http://www.d3noob.org/2012/12/setting-scales-domains-and-ranges-in.html>
hBird.yScale = d3.scale.linear()
                .domain([0, 100])
                .range([0, hBird.sizes.height - hBird.sizes.margin.bottom]);

hBird.yCoords = function(data) {
  var intermed = [0].concat(
    data.map(
      hBird.addValue(hBird.yScale)
    )
  );
  return hBird.yCoords.coords = intermed.splice(0, intermed.length-1);
};

hBird.median = function(i) {
  var coords = hBird.yCoords.coords,
      next = i === coords.length-1 ? (hBird.sizes.height - hBird.sizes.margin.bottom) : coords[i+1];
  return (hBird.yCoords.coords[i] + next) / 2;
}

hBird.initialize = function(cb, config) {
      var yC = hBird.yCoords(config.data.values);
    var size = hBird.sizes,
      canvas = d3.select(".chart")
                 .append("svg");
  canvas.attr("viewBox", "0 0 762 675"/* + size.calcWidth(canvas.style("width")) + " " + size.calcHeight(canvas.style("height")) */)
        .attr("preserveAspectRatio", "xMinYMin meet");
                 //.attr("height", size.height - size.margin.top - size.margin.bottom);
    /* Gradients */
    var defs = canvas.append("defs"),
   tempGradient = defs.append("linearGradient")
                .attr("id", "gradient0");
   tempGradient.append("stop")
                .attr("stop-color", "rgb(63,108,122)")
                .attr("offset", "0");
   tempGradient.append("stop")
                .attr("stop-color", "rgb(81,140,163)")
                .attr("offset", "100%");
   tempGradient = defs.append("linearGradient")
                .attr("id", "gradient1");
   tempGradient.append("stop")
                .attr("stop-color", "rgb(0,62,86)")
                .attr("offset", "0");
   tempGradient.append("stop")
                .attr("stop-color", "rgb(0,87,114)")
                .attr("offset", "100%");
   tempGradient = defs.append("linearGradient")
                .attr("id", "gradient2");
   tempGradient.append("stop")
                .attr("stop-color", "rgb(12,137,175)")
                .attr("offset", "0");
   tempGradient.append("stop")
                .attr("stop-color", "rgb(13,163,221)")
                .attr("offset", "100%");
   tempGradient = defs.append("linearGradient")
                .attr("id", "gradient3");
   tempGradient.append("stop")
                .attr("stop-color", "rgb(81,137,150)")
                .attr("offset", "0");
   tempGradient.append("stop")
                .attr("stop-color", "rgb(107,184,224)")
                .attr("offset", "100%");
   tempGradient = defs.append("linearGradient")
                .attr("id", "gradient4");
   tempGradient.append("stop")
                .attr("stop-color", "rgb(81,81,81)")
                .attr("offset", "0");
   tempGradient.append("stop")
                .attr("stop-color", "rgb(109,109,109)")
                .attr("offset", "100%");
   defs = tempGradient = null;


                //.insert("stop-color", "rgb(81,140,163)");
size.barWidth = 762 - size.margin.right - size.margin.left;//size.getWidth(canvas.style("width")) - size.margin.right - size.margin.left;
     var rect = canvas
                .selectAll("rect")
                .data(config.data.values)
                .enter()                
                .append("g") 
                .attr("class", "rects")              
                .attr("transform", "translate(" + size.margin.left + ")")
                .append("rect")
                .attr("fill", function(d, i) {
                  return "url(#gradient" + i + ")";
                })
                .attr("y", function(d, i) {
                  return yC[i];
                })
                .attr("height", function(d, i) {
                  return hBird.yScale(d.value);
                })
                .attr("width", size.barWidth)
                .attr("class", function(d, i) {
                  return "segment" + (i + 1);
                })
                .each(function(d, i) {
                  this._yCoord = yC[i];
                  this._datum = d;
                });
var lineToLabels = canvas
                .selectAll("g.rects")
                .data(config.data.values)
                .append("line")
                .attr("x1", 0)
                .attr("y1", function(d, i) {
                  return hBird.median(i);
                })
                .attr("x2", size.margin.legendRight)
                .attr("y2", function(d, i) {
                  return hBird.median(i)-30;
                })
                .attr("stroke-width", 1)
                .attr("transform", "translate(" + size.barWidth + ")")
                .attr("class", "line-to-label")
                .each(function(d) {
                  if(!d.value)
                    d3.select(this).attr("opacity", 0);
                });
   var legend = canvas
                .selectAll("g.rects")
                .data(config.data.values)
                .append("g")
                //.attr("transform", "translate(" + (rectWidth + size.margin.legendRight + 22) + ")");
                .attr("transform", function(d, i) {
                  var x = size.barWidth + size.margin.legendRight + 22;
                  return "translate(" + x + ", " + hBird.median(i) + ")";
                })
                .each(function(d) {
                  if(!d.value)
                    d3.select(this).attr("opacity", 0);
                });
var percentage = legend
                .append("text")
                .text(function(d) {
                  return Math.round(d.value) + "%";
                })
                .attr("y", -36)
                .attr("fill", "rgb(107,184,214)")
                .attr("class", "percentage-svg");
    var icons = legend
                .attr("d", function(d) {
                  var iconName = hBird.iconTable[d.label.trim()];
                  var icon = d3.select(config.iconset).select("#" + iconName);
                  icon.attr("class", iconName);
                  switch (iconName) {
                    case "drop":
                      icon.attr("transform", "translate(5, -76)");
                      break;
                    case "pipette":
                      icon.attr("transform", "translate(-26, -79)");
                      break;                    
                    case "question":
                      icon.attr("transform", "translate(-10, -80)");
                      break;
                    case "pill":
                      icon.attr("transform", "translate(-8, -80)");
                      break;
                    case "cross":
                      icon.attr("transform", "translate(-8, -81)");
                      break;
                    default:
                      icon.attr("transform", "translate(0, -69)");
                  }
                  this.appendChild(icon.node()/*.cloneNode(true)*/);
                });
   var labels = legend
                .append("text")
                .text(function(d) {
                  return d.label;
                })
                .attr("y", -10)
                .attr("fill", "#000")
                .attr("text-anchor", "right")
                .attr("class", "label");
    var yAxis = d3.svg.axis()
                .scale(hBird.yDownUpScale)
                .tickSize(1)/*
                .tickPadding(-4)
                .innerTickSize(2)*/
                .orient("left")
                .tickValues([0, 20, 40, 60, 80, 100])
                .tickFormat(function(d) {
                  return d + "%";
                });
  canvas.append("g")
        .attr("class", "y-axis")
        .attr("transform", "translate(60)") /* minimum to see text */
        .call(yAxis);

  var survivalRate = d3.select(".lost .number");
  survivalRate
    .datum(config.data["Survival Rate"])
    .transition()
    .duration(750)
    .delay(100)
    .tween("text", function(d) {
      var orig = +this.textContent,
          inter= d3.interpolateRound(orig, d);
      return function(t) {
        this.textContent = inter(t);
      }
    });
  var periodText = d3.select(".year")
                     .text(config.data.period);

  config.canvas = canvas;
  config.rect = rect;
  config.periodText = periodText;
  config.legend = legend;
  config.labels = labels;
  config.percentage = percentage;
  config.lineToLabels = lineToLabels;
  config.survivalRate = survivalRate;
  cb(config);
};

hBird.cleanup = function(cb, config) {
  config.iconset = undefined;
  cb(config);
}

hBird.draw = function draw(cb, config) {
  hBird.yCoords(config.data.values);
  config.rect
        .data(config.data.values)
        .transition()
        .duration(750)
        .attrTween("y", function(d, i) {
          var inter = d3.interpolate(this._yCoord, hBird.yCoords.coords[i]);
          this._yCoord = hBird.yCoords.coords[i];
          return inter;
        })
        .attrTween("height", function(d) {
          var inter = d3.interpolate(hBird.yScale(this._datum.value), hBird.yScale(d.value));
          this._datum = d;
          return inter;
        })
        .each(function(d, i) {
          d3.select(this)
            .attr("height", hBird.yScale(d.value))
            .attr("y", hBird.yCoords.coords[i]);
        });
  
  config.periodText.text(config.data.period);
  config.legend
        .data(config.data.values)
        .transition()
        .duration(750)
        .attr("transform", function(d, i) {
          var size = hBird.sizes;
          var x = size.barWidth + size.margin.legendRight + 22;
          return "translate(" + x + ", " + hBird.median(i) + ")";
        })
        .attr("opacity", function(d) {
          return d.value ? 1 : 0;
        })
        .select(".drop")
        .attr("transform", function(d) {
          return d.value === 100 ? "translate(2, -76)" : "translate(-14, -76)";
        });
  config.percentage
        .data(config.data.values)
        .transition()
        .duration(750)
        .tween("text", function(d) {
          var orig = +this.textContent.replace("%", ""),
              //inter= d3.interpolateNumber(orig, d.value),
              //isInt= d.value % 1 === 0;
              inter= d3.interpolateRound(orig, d.value);
              
          return function(t) {
            var display = inter(t);
            //this.textContent = (isInt ? inter(t).toFixed(0) : inter(t).toFixed(1)) + "%";
            this.textContent = inter(t) + "%";
          }
        });
  config.lineToLabels
        .data(config.data.values)
        .transition()
        .duration(750)
        .attr("y1", function(d, i) {
          return hBird.median(i);
        })
        .attr("y2", function(d, i) {
          // return the median between the current and next coord
          return hBird.median(i)-30;
        })
        .attr("opacity", function(d) {
          return d.value ? 1 : 0;
        });
  
  config.survivalRate
    .datum(config.data["Survival Rate"])
    .transition()
    .duration(750)
    .delay(260)
    .tween("text", function(d) {
      var orig = +this.textContent,
          inter= d3.interpolateRound(orig, d);
      return function(t) {
        this.textContent = inter(t);
      }
    });

  cb(config);
};

hBird.log = function log(cb, data) {
  console.dir(data);
  cb(data);
};

hBird.previous = function previous(config, back, forw) {
  return function() {
    // this.style.cursor = "progress";
    // drawing.transition().duration(0);
    hBird.move(-1, config, back, forw);
    // this.style.cursor = "pointer";
  }
};

hBird.next = function next(config, back, forw) {
  return function() {
    // this.style.cursor = "progress";
    // drawing.transition().duration(0);
    hBird.move(1, config, back, forw);
    // this.style.cursor = "pointer";
  }
};

hBird.update = function(object, giveprop, takeprop, fn) {
  var argumentsLength = arguments.length;
  return function update(cb, updated) {
    switch (argumentsLength) {
      case 1: object = updated; break;
      case 2: object[giveprop] = updated; break;
      case 3: object[giveprop] = updated[takeprop]; break;
      case 4: object[giveprop] = fn(updated[takeprop]); break;
      default: console.warn("hBird.update called with weird arguments");
    }
    cb(object);
  }
};

hBird.move = function(direction, config, backwards, forwards) {
  var test = config.counter.c;
  test += direction;
  if(test < 0 || test >= config.counter.max) {
    console.warn("we got to the end!")
    return; // we at the end
  }
  !~direction ? hBird.defer(backwards(config)) : hBird.defer(forwards(config));
};

hBird.pickData = function(direction, counter) {
  return function pickData(cb, config) {
    config.data = config.dataset[[!~direction ? --counter.c : ++counter.c]];
    cb(config);
  }
};

// Needs further thought
// hBird.indentity = function(cb, data) {
//   cb(cb(data), data);
// };
hBird.wait = function(wait) {
  return function wait(cb, config) {
    setTimeout(function() {
      cb(config);
    }, wait)
  }
};

hBird.defer = function defer() {
  return hBird.wait(0);
}

hBird.noOp = function noOp(cb, config) { cb; };

(function() {
  var counter = { c:-1, max: 0 },//TODO 0 from url .. -1 is a hack because forwardDataPicker otherwise will initialize the 1st dataset and not the 0th
      forwardDataPicker = hBird.pickData(1, counter),
      backwardsDataPicker = hBird.pickData(-1, counter);

  var svgUrl = function(cb, config) {
    hBird.update(config, "url")(cb, "images/iconset_stacked.svg");
  }

  /* performance measurement */
  // var start = function(cb, config) {
  //   config.start = window.performance.now();
  //   cb(config);
  // };
  // var end = function(cb, config) {
  //   console.log( window.performance.now() - config.start + "ms");
  //   cb(config);
  // };

  // setup program
  config = new hBird.create({
    lineToLabels: undefined,
    legend: undefined,
    percentage: undefined,
    labels: undefined,
    periodText: undefined,
    url: "data/data10.json",
    dataset: undefined,
    data: undefined,
    canvas: undefined,
    rect: undefined,
    counter:counter,
    iconset: undefined,
    survivalRate: undefined,
    start: undefined
  });
  initialRun = hBird.compose(
   // start,
    hBird.load,
    hBird.update(config, "dataset", "data"),
    forwardDataPicker,
    svgUrl,
    hBird.load,
    hBird.initialize,
  //  end,
    hBird.update(counter, "max", "dataset", function(d) {
      return d.length;
    }),
    hBird.cleanup    
  );
  forwards = hBird.composeCycle(
  //  start,
    forwardDataPicker,
    hBird.draw,
  //  end,
    hBird.noOp
  );
  backwards = hBird.composeCycle(
  //  start,
    backwardsDataPicker,
    hBird.draw,
  //  end,
    hBird.noOp
  );

  d3.select(".previous")
    .on("click", hBird.previous(config, backwards, forwards));
  d3.select(".next")
    .on("click", hBird.next(config, backwards, forwards));

  // start program
  initialRun(config);
}());
