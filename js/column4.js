var hBird = hBird || {};

hBird.config = function config(config) {
  for(var prop in config) {
    if(config.hasOwnProperty(prop))
      this[prop] = config[prop];
  }
}

hBird.load = function(cb, config) {
  d3.json(config.url, function(error, data) {
    if(error)
      return console.warn(error)
    config.data = data;
    cb(config);
  });
}

hBird.addValue = function(modifier) {
  var firstValue = 0;
  if(modifier && !(modifier instanceof Function))
    throw "modifier must be a function"
  return function(item) {
    return modifier ?
            modifier(firstValue += item.value) :
            firstValue += item.value;
  }
}
hBird.yScale = d3.scale.linear()
                .domain([0, 100])
                .range([0, 400]);
hBird.color = [
                d3.rgb(251, 255, 255),
                d3.rgb(215, 227, 223),
                d3.rgb(197, 218, 161),
                d3.rgb(188, 202, 1),
                d3.rgb(9, 43, 52)
              ];
hBird.yCoords = function(data) {
  var intermed = [0].concat(
    data.map(
      hBird.addValue(hBird.yScale)
    )
  );
  return hBird.yCoords.coords = intermed.splice(0, intermed.length-1);
};

hBird.initialize = function(cb, config) {
  hBird.yCoords(config.data.values);
  config.canvas = d3.select(document.body)
                .append("svg")
                .attr("width", 100)
                .attr("height", 400),
  config.rect = config.canvas.selectAll("rect")
               .data(config.data.values)
               .enter()
               .append("rect")
               .attr("y", function(d, i) {
                 return hBird.yCoords.coords[i];
               })
               .attr("height", function(d, i) {
                 return hBird.yScale(d.value);
               })
               .attr("width", 100)
               .attr("fill", function(d, i) {
                 return hBird.color[i];
               })
               .each(function(d, i) {
                 this._yCoord = hBird.yCoords.coords[i];
                 this._datum = d;
               });

  cb(config);
}

hBird.pickData = function(start) {
  var currentDataSet;
  start = start || 0;
  return function(cb, config) {
    if(!currentDataSet)
      currentDataSet = config.data;
    config.data = currentDataSet[start++];
    cb(config);
  }
};

hBird.draw = function(cb, config) {
  hBird.yCoords(config.data.values);
  config.rect
        .data(config.data.values)
        .transition()
        .duration(750)
        .attrTween("y", function(d, i) {
          var inter = d3.interpolate(this._yCoord, hBird.yCoords.coords[i]);
          this._yCoord = hBird.yCoords.coords[i];
          return inter;
        })
        .attrTween("height", function(d) {
          var inter = d3.interpolate(hBird.yScale(this._datum.value), hBird.yScale(d.value));
          this._datum = d;
          return inter;
        })
        .each(function(d, i) {
          d3.select(this)
            .attr("height", hBird.yScale(d.value))
            .attr("y", hBird.yCoords.coords[i]);
        });
  cb(config);
};

hBird.log = function log(cb, data) {
  console.dir(data);
  cb(data);
};

hBird.indentity = function(cb, data) {
  return cb(data);
};
hBird.wait = function(wait) {
  return function(cb, config) {
    setTimeout(function() {
      cb(config);
    }, wait)
  }
};

var dataPicker = hBird.pickData(0);
var debugRun = hBird.compose(hBird.load, dataPicker, hBird.initialize, hBird.log, hBird.wait(2000), dataPicker, hBird.draw, hBird.log);
var firstRun = hBird.compose(
  hBird.load,
  dataPicker,
  hBird.initialize,
  hBird.wait(2000),
  dataPicker,
  hBird.draw,
  hBird.wait(2000),
  dataPicker,
  hBird.draw,
  hBird.wait(2000),
  dataPicker,
  hBird.draw,
  hBird.wait(2000),
  dataPicker,
  hBird.draw,
  hBird.wait(2000),
  dataPicker,
  hBird.draw  
);
firstRun(new hBird.config({ url: "data/data6.json", data: null, canvas: null, rect: null }));

