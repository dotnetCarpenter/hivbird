/**
 * @version 2003.9.1
 * @author jon.ronnenberg@gmail.com
 */

var hBird = hBird || {};

hBird.create = function create(config) {
  for(var prop in config) {
    if(config.hasOwnProperty(prop))
      this[prop] = config[prop];
  }
};

hBird.load = function(cb, config) {
  d3.json(config.url, function(error, data) {
    if(error)
      return console.warn(error)
    config.data = data;
    cb(config);
  });
};

hBird.addValue = function(modifier) {
  var firstValue = 0;
  if(modifier && !(modifier instanceof Function))
    throw "modifier must be a function";
  return function(item) {
    return modifier ?
            modifier(firstValue += item.value) :
            firstValue += item.value;
  }
};

hBird.yScale = d3.scale.linear()
                .domain([0, 100])
                .range([0, 400]);

hBird.yCoords = function(data) {
  var intermed = [0].concat(
    data.map(
      hBird.addValue(hBird.yScale)
    )
  );
  return hBird.yCoords.coords = intermed.splice(0, intermed.length-1);
};

hBird.initialize = function(cb, config) {
  hBird.yCoords(config.data.values);
  var canvas = d3.select(".chart")
                .append("svg"),
      width  = canvas.style("width"); 
               canvas
                .attr("width", width)
                .attr("height", 400);
    var rect = canvas.selectAll("rect")
                .data(config.data.values)
                .enter()
                .append("rect")
                .attr("y", function(d, i) {
                  return hBird.yCoords.coords[i];
                })
                .attr("height", function(d, i) {
                  return hBird.yScale(d.value);
                })
                .attr("width", "100%")
                .attr("class", function(d, i) {
                  return "segment" + (i + 1);
                })
                .each(function(d, i) {
                  this._yCoord = hBird.yCoords.coords[i];
                  this._datum = d;
                });
  var previous = d3.select(".previous")
                  .on("click", hBird.previous(rect));
  var next     = d3.select(".next")
                  .on("click", hBird.next(rect));
  config.canvas = canvas;
  config.rect = rect;
  cb(config);
};

hBird.draw = function draw(cb, config) {
  hBird.yCoords(config.data.values);
  config.rect
        .data(config.data.values)
        .transition()
        .duration(750)
        .attrTween("y", function(d, i) {
          var inter = d3.interpolate(this._yCoord, hBird.yCoords.coords[i]);
          this._yCoord = hBird.yCoords.coords[i];
          return inter;
        })
        .attrTween("height", function(d) {
          var inter = d3.interpolate(hBird.yScale(this._datum.value), hBird.yScale(d.value));
          this._datum = d;
          return inter;
        })
        .each(function(d, i) {
          d3.select(this)
            .attr("height", hBird.yScale(d.value))
            .attr("y", hBird.yCoords.coords[i]);
        });
  cb(config);
};

hBird.log = function log(cb, data) {
  console.dir(data);
  cb(data);
};

hBird.previous = function previous(drawing) {
  return function() {
    // this.style.cursor = "progress";
    // drawing.transition().duration(0);
    hBird.move(-1);
    // this.style.cursor = "pointer";
  }
};

hBird.next = function next(drawing) {
  return function() {
    // this.style.cursor = "progress";
    // drawing.transition().duration(0);
    hBird.move(1);
    // this.style.cursor = "pointer";
  }
};

hBird.update = function(object, giveprop, takeprop, fn) {
  var argumentsLength = arguments.length;
  return function update(cb, updated) {
    switch (argumentsLength) {
      case 1: object = updated; break;
      case 2: object[giveprop] = updated; break;
      case 3: object[giveprop] = updated[takeprop]; break;
      case 4: object[giveprop] = fn(updated[takeprop]); break;
      default: console.warn("hBird.update called with weird arguments");
    }
    cb(updated);
  }
};

hBird.move = function(direction) {
  var test = hBird.config.counter.c;
  test += direction;
  if(test < 0 || test >= hBird.config.counter.max) {
    console.warn("we got to the end!")
    return; // we at the end
  }
  !~direction ? hBird.defer(hBird.backwards(hBird.config)) : hBird.defer(hBird.forwards(hBird.config));
};

hBird.pickData = function(direction, counter) {
  return function pickData(cb, config) {
    config.data = config.dataset[[!~direction ? --counter.c : ++counter.c]];
    cb(config);
  }
};

// Needs further thought
// hBird.indentity = function(cb, data) {
//   cb(cb(data));
// };
hBird.wait = function(wait) {
  return function wait(cb, config) {
    setTimeout(function() {
      cb(config);
    }, wait)
  }
};

hBird.defer = function defer() {
  return hBird.wait(0);
}

hBird.noOp = function noOp(cb, config) { cb; };

(function() {
  var counter = { c:-1, max: 0 },//TODO 0 from url .. -1 is a hack because forwardDataPicker otherwise will initialize the 1st dataset and not the 0th
      forwardDataPicker = hBird.pickData(1, counter),
      backwardsDataPicker = hBird.pickData(-1, counter);

  // setup program
  hBird.config = new hBird.create({ url: "data/data7.json", dataset: null, data: null, canvas: null, rect: null, counter:counter });
  hBird.initialRun = hBird.compose(
    hBird.load,
    hBird.update(counter, "max", "data", function(d) {
      return d.length
    }),
    hBird.update(hBird.config, "dataset", "data"),
    forwardDataPicker,
    hBird.initialize
  );
  hBird.forwards = hBird.composeCycle(
    forwardDataPicker,
    hBird.draw,
    hBird.noOp
  );
  hBird.backwards = hBird.composeCycle(
    backwardsDataPicker,
    hBird.draw,
    hBird.noOp
  );
  // start program
  hBird.initialRun(hBird.config);
}());



// // start program
// hBird.start();
// var dataPicker = hBird.pickData(0);
// var debugRun = hBird.compose(hBird.load, dataPicker, hBird.initialize, hBird.log, hBird.wait(2000), dataPicker, hBird.draw, hBird.log);
// var animateRun = hBird.compose(
//   hBird.load,
//   dataPicker,
//   hBird.initialize,
//   hBird.wait(2000),
//   dataPicker,
//   hBird.draw,
//   hBird.wait(2000),
//   dataPicker,
//   hBird.draw,
//   hBird.wait(2000),
//   dataPicker,
//   hBird.draw,
//   hBird.wait(2000),
//   dataPicker,
//   hBird.draw,
//   hBird.wait(2000),
//   dataPicker,
//   hBird.draw  
// );

// animateRun(new hBird.config({ url: "data/data7.json", data: null, canvas: null, rect: null }));
